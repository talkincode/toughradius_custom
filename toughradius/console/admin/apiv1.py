#!/usr/bin/env python
# coding=utf-8
from twisted.python import log
from bottle import Bottle
from bottle import request
from bottle import response
from bottle import redirect
from bottle import static_file
from bottle import mako_template as render
from tablib import Dataset
from beaker.cache import cache_managers
from toughradius.console.websock import websock
from toughradius.console import models
from toughradius.console.libs import utils
from toughradius.console.base import *
from toughradius.console.admin import forms
from hashlib import md5
import bottle
import datetime
import json
import time
import decimal
from sqlalchemy import func
import traceback

__prefix__ = "/api/v1"

app = Bottle()
app.config['__prefix__'] = __prefix__

decimal.getcontext().prec = 11
decimal.getcontext().rounding = decimal.ROUND_UP

def mksign(secret, params=[]):
    _params = [str(p) for p in params if p is not None]
    _params.sort()
    _params.insert(0, secret)
    strs = ''.join(_params)
    log.msg("sign_src = %s" % strs)
    mds = md5(strs.encode()).hexdigest()
    return mds



def clear_cache():
    for _cache in cache_managers.values():
        _cache.clear()
    websock.update_cache("all", callback=None)


@app.post('/userAuth')
def user_auth(db,render):
    req_msg = {}
    try:
        req_msg = json.loads(request.body.getvalue())
    except Exception as err:
        traceback.print_exc()
        log.err('parse params error %s' % str(err))
        return dict(code=1, msg='parse params error')

    node_id = req_msg.get('node')
    account_number = req_msg.get('username')
    realname = req_msg.get('realname')
    password = req_msg.get('password')
    session_time = req_msg.get('session_time')
    idcard = req_msg.get('idcard')
    mobile = req_msg.get('mobile')
    address = req_msg.get('address')
    ipaddr = req_msg.get('ipaddr')
    product_id = req_msg.get('product')
    months = req_msg.get('months')
    fee = req_msg.get('fee')
    expire = req_msg.get('expire')
    desc = req_msg.get('desc')
    sign = req_msg.get('sign')

    try:

        params = [node_id, account_number, realname, password, session_time, idcard,
                  mobile, address, ipaddr, product_id, months, fee,expire,desc]
        _sign = mksign(app.config['DEFAULT.secret'], params)
        print _sign
        if sign not in _sign:
            return dict(code=1, msg='sign error')

        if db.query(models.SlcRadAccount).filter_by(account_number=account_number).count() > 0:
            return dict(code=1, msg=u"user %s exists" % account_number)

        if ipaddr and db.query(models.SlcRadAccount).filter_by(ip_address=ipaddr).count() > 0:
            return dict(code=1, msg=u"ipaddr %s is used" % ipaddr)

        member = models.SlcMember()
        member.node_id = node_id
        member.realname = realname or account_number
        member.member_name = account_number
        member.password = md5(password.encode()).hexdigest()
        member.idcard = idcard or '000000'
        member.sex = '1'
        member.age = '0'
        member.email = ''
        member.mobile = mobile or '000000'
        member.address = address
        member.create_time = utils.get_currtime()
        member.update_time = utils.get_currtime()
        member.email_active = 0
        member.mobile_active = 0
        member.active_code = utils.get_uuid()
        member.member_desc = desc
        db.add(member)
        db.flush()
        db.refresh(member)

        accept_log = models.SlcRadAcceptLog()
        accept_log.accept_type = 'open'
        accept_log.accept_source = 'console'
        accept_log.account_number = account_number
        accept_log.accept_time = member.create_time
        accept_log.operator_name = "admin"
        accept_log.accept_desc = u"用户新开户：(%s)%s" % (member.member_name, member.realname)
        db.add(accept_log)
        db.flush()
        db.refresh(accept_log)

        order_fee = 0
        balance = 0
        expire_date = expire
        product = db.query(models.SlcRadProduct).get(product_id)
        # 预付费包月
        if product.product_policy == 0:
            order_fee = decimal.Decimal(product.fee_price) * decimal.Decimal(months)
            order_fee = int(order_fee.to_integral_value())
        # 买断包月,买断流量
        elif product.product_policy in (2, 5):
            order_fee = int(product.fee_price)
        # 预付费时长,预付费流量
        elif product.product_policy in (1, 4):
            balance = utils.yuan2fen(fee)
            expire_date = '3000-11-11'

        order = models.SlcMemberOrder()
        order.order_id = utils.gen_order_id()
        order.member_id = member.member_id
        order.product_id = product.id
        order.account_number = account_number
        order.order_fee = order_fee
        order.actual_fee = utils.yuan2fen(fee)
        order.pay_status = 1
        order.accept_id = accept_log.id
        order.order_source = 'api'
        order.create_time = member.create_time
        order.order_desc = u"用户新开账号"
        db.add(order)

        account = models.SlcRadAccount()
        account.account_number = account_number
        account.ip_address = ipaddr
        account.member_id = member.member_id
        account.product_id = order.product_id
        account.install_address = member.address
        account.mac_addr = ''
        account.password = utils.encrypt(password)
        account.status = 1
        account.balance = balance
        account.time_length = int(product.fee_times)
        account.flow_length = int(product.fee_flows)
        account.expire_date = expire_date
        account.user_concur_number = product.concur_number
        account.bind_mac = product.bind_mac
        account.bind_vlan = product.bind_vlan
        account.vlan_id = 0
        account.vlan_id2 = 0
        account.create_time = member.create_time
        account.update_time = member.create_time
        account.account_desc = member.member_desc
        db.add(account)

        if int(session_time or 0) > 0:
            attr = models.SlcRadAccountAttr()
            attr.account_number = account.account_number
            attr.attr_name = 'Session-Timeout'
            attr.attr_value = int(session_time)
            attr.attr_desc = u'最大会话时长'
            db.add(attr)

        db.commit()
        return dict(code=0, msg='success')

    except Exception as err:
        traceback.print_exc()
        log.err('userAuth error %s' % str(err))
        return dict(code=1, msg='userAuth error %s' % str(err))


@app.post('/userUnAuth')
def user_unauth(db,render):
    req_msg = {}
    try:
        req_msg = json.loads(request.body.getvalue())
    except Exception as err:
        log.err('parse params error %s' % str(err))
        return dict(code=1, msg='parse params error')

    account_number = req_msg.get('username')
    sign = req_msg.get('sign')

    _sign = mksign(app.config['DEFAULT.secret'], [account_number])
    print _sign
    if sign not in _sign:
        return dict(code=1, msg='sign error')

    try:
        db.query(models.SlcMember).filter_by(member_name=account_number).delete()
        db.query(models.SlcRadAcceptLog).filter_by(account_number=account_number).delete()
        db.query(models.SlcRadAccountAttr).filter_by(account_number=account_number).delete()
        db.query(models.SlcRadBilling).filter_by(account_number=account_number).delete()
        db.query(models.SlcRadTicket).filter_by(account_number=account_number).delete()
        db.query(models.SlcRadOnline).filter_by(account_number=account_number).delete()
        db.query(models.SlcRechargeLog).filter_by(account_number=account_number).delete()
        db.query(models.SlcRadAccount).filter_by(account_number=account_number).delete()
        db.query(models.SlcMemberOrder).filter_by(account_number=account_number).delete()

        ops_log = models.SlcRadOperateLog()
        ops_log.operator_name = "admin"
        ops_log.operate_ip = request.remote_addr
        ops_log.operate_time = utils.get_currtime()
        ops_log.operate_desc = u'反授权用户%s' % (account_number)
        db.add(ops_log)

        db.commit()
        clear_cache()
        return dict(code=0, msg='success')

    except Exception as err:
        traceback.print_exc()
        log.err('userUnAuth error %s' % str(err))
        return dict(code=1, msg='userUnAuth error %s' % str(err))


@app.post('/onlineQuery')
def user_online_query(db,render):
    req_msg = {}
    try:
        req_msg = json.loads(request.body.getvalue())
    except Exception as err:
        log.err('parse params error %s' % str(err))
        return dict(code=1, msg='parse params error')

    node_id = req_msg.get('node_id')
    account_number = req_msg.get('username')
    nasaddr = req_msg.get('nasaddr')
    ipaddr = req_msg.get('ipaddr')
    macaddr = req_msg.get('macaddr')
    product_id = req_msg.get('product')
    pagesize = req_msg.get('pagesize')
    pagenum = req_msg.get('pagenum')
    sign = req_msg.get('sign')

    params = [node_id, account_number, ipaddr, nasaddr, macaddr, product_id, pagesize, pagenum]
    _sign = mksign(app.config['DEFAULT.secret'], params)
    if sign not in _sign:
        return dict(code=1, msg='sign error')

    pagesize = pagesize and int(pagesize) or 50
    pagenum = pagenum and int(pagenum) or 1
    offset = (pagenum - 1) * pagesize

    try:
        _query = db.query(
            models.SlcRadOnline.id,
            models.SlcRadOnline.account_number,
            models.SlcRadOnline.nas_addr,
            models.SlcRadOnline.acct_session_id,
            models.SlcRadOnline.acct_start_time,
            models.SlcRadOnline.framed_ipaddr,
            models.SlcRadOnline.mac_addr,
            models.SlcRadOnline.nas_port_id,
            models.SlcRadOnline.start_source,
            models.SlcRadOnline.billing_times,
            models.SlcRadOnline.input_total,
            models.SlcRadOnline.output_total,
            models.SlcMember.node_id,
            models.SlcMember.realname
        ).filter(
            models.SlcRadOnline.account_number == models.SlcRadAccount.account_number,
            models.SlcMember.member_id == models.SlcRadAccount.member_id
        )
        if node_id:
            _query = _query.filter(models.SlcMember.node_id == node_id)
        if account_number:
            _query = _query.filter(models.SlcRadOnline.account_number.like('%' + account_number + '%'))
        if ipaddr:
            _query = _query.filter(models.SlcRadOnline.framed_ipaddr == ipaddr)
        if macaddr:
            _query = _query.filter(models.SlcRadOnline.mac_addr == macaddr)
        if nasaddr:
            _query = _query.filter(models.SlcRadOnline.nas_addr == nasaddr)

        _query = _query.order_by(models.SlcRadOnline.acct_start_time.desc())
        total = _query.count()
        _query = _query.limit(pagesize).offset(offset)

        result = dict(code=0, msg="success", total=total)
        onlines = []
        for o in _query:
            _online = dict(
                username=o.account_number,
                nasaddr=o.nas_addr,
                sessionid=o.acct_session_id,
                starttime=o.acct_start_time,
                ipaddr=o.framed_ipaddr,
                macaddr=o.mac_addr,
                input=o.input_total,
                output=o.output_total
            )
            onlines.append(_online)
        result['onlines'] = onlines
        return result
    except Exception as err:
        log.err('onlineQuery error %s' % str(err))
        return dict(code=1, msg='onlineQuery error %s' % str(err))


@app.post('/userUnlock')
def user_unlock(db,render):
    req_msg = {}
    try:
        req_msg = json.loads(request.body.getvalue())
    except Exception as err:
        log.err('parse params error %s' % str(err))
        return dict(code=1, msg='parse params error')

    account_number = req_msg.get('username')
    ipaddr = req_msg.get('ipaddr')
    macaddr = req_msg.get('macaddr')
    sign = req_msg.get('sign')

    params = [p for p in (account_number, ipaddr, macaddr) if p]
    _sign = mksign(app.config['DEFAULT.secret'], params)
    print _sign
    if sign not in _sign:
        return dict(code=1, msg='sign error')

    if not any([account_number, ipaddr, macaddr]):
        return dict(code=1, msg=u'params [username,ipaddr,macaddr] must have at least one')

    query = db.query(models.SlcRadOnline)
    if account_number:
        query = query.filter(models.SlcRadOnline.account_number == account_number)

    if ipaddr:
        query = query.filter(models.SlcRadOnline.framed_ipaddr == ipaddr)

    if macaddr:
        query = query.filter(models.SlcRadOnline.mac_addr == macaddr)

    online = query.first()
    if not online:
        return dict(code=0, msg='online user not find')

    def disconn():
        websock.invoke_admin("coa_request",
                             nas_addr=online.nas_addr,
                             acct_session_id=online.acct_session_id,
                             message_type='disconnect'
                             )

    def is_ok():
        return db.query(models.SlcRadOnline).filter_by(acct_session_id=online.acct_session_id).count() == 0

    disconn()
    count = 0
    while count < 3:
        if is_ok():
            return dict(code=0, msg='success')
        else:
            disconn()
            time.sleep(0.1)
            count += 1

    if not is_ok():
        return dict(code=1, msg='unlock user fail,retry time %s' % count)

    return dict(
        code=0,
        msg='success'
    )


@app.post('/userRelease')
def user_release(db,render):
    req_msg = {}
    try:
        req_msg = json.loads(request.body.getvalue())
    except Exception as err:
        log.err('parse params error %s' % str(err))
        return dict(code=1, msg='parse params error')

    account_number = req_msg.get('username')
    sign = req_msg.get('sign')

    _sign = mksign(app.config['DEFAULT.secret'], [account_number])
    if sign not in _sign:
        return dict(code=1, msg='sign error')

    try:
        user = db.query(models.SlcRadAccount).filter_by(account_number=account_number).first()
        user.mac_addr = ''
        user.vlan_id = 0
        user.vlan_id2 = 0

        ops_log = models.SlcRadOperateLog()
        ops_log.operator_name = "admin"
        ops_log.operate_ip = request.remote_addr
        ops_log.operate_time = utils.get_currtime()
        ops_log.operate_desc = u'释放用户账号（%s）绑定信息' % (account_number,)
        db.add(ops_log)

        db.commit()
        clear_cache()
        return dict(code=0, msg='success')

    except Exception as err:
        log.err('userRelease error %s' % str(err))
        return dict(code=1, msg='userRelease error %s' % str(err))